import {Component, OnInit} from '@angular/core';
import {Block} from '../../../shared/models/block.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public json = [
    {
      title: 'Heading',
      subtitle: 'Lorem ipsum dolor sit amet.',
      image: '../../../../assets/image/logo-wordpress.png'
    },
    {
      title: 'Heading',
      subtitle: 'Lorem ipsum dolor sit amet, consect \n etur adipiscing elit. Aenea uismod \n bibendum laoreet. Proin gravida dolor \n sit amet lacus.'
    },
    {
      title: 'Ultra Mega Super Long \n Heading',
      subtitle: 'Lorem ipsum dolor sit amet, consect \n etur adipiscing elit. Aenea uismod \n bibendum laoreet. Lorem ipsum dolor \n sit amet'
    },
    {
      title: 'Ultra Mega Super',
      subtitle: 'Lorem ipsum dolor sit amet, consect \n etur adipiscing elit. Aenea uismod'
    }];
  public json2 = [
    {
      title: 'Heading',
      subtitle: 'Lorem ipsum dolor sit amet.',
      image: '../../../../assets/image/fire.png'
    },
    {
      title: 'Heading',
      subtitle: 'Lorem ipsum dolor sit amet, consect \n etur adipiscing elit. Aenea uismod \n bibendum laoreet. Proin gravida dolor \n sit amet lacus.'
    },
    {
      title: 'Ultra Mega Super Long \n Heading',
      subtitle: 'Lorem ipsum dolor sit amet, consect \n etur adipiscing elit. Aenea uismod \n bibendum laoreet. Lorem ipsum dolor \n sit amet'
    },
  ];
  public blocks: Block[];
  public blocks2: Block[];

  constructor() {
  }

  ngOnInit(): void {
    this.blocks = this.json.map(block => new Block(block));
    this.blocks2 = this.json2.map(block => new Block(block));
  }
}
