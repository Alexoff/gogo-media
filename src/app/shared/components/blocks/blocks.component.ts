import {Component, Input, OnInit} from '@angular/core';
import {Block} from '../../models/block.model';

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.component.html',
  styleUrls: ['./blocks.component.scss'],
  host: {
    '(window:resize)': 'onWindowResize($event)'
  }
})
export class BlocksComponent implements OnInit {
  @Input() blocks: Block[];
  public isSlider: number;
  public width: number = window.innerWidth;
  public step = 0;

  constructor() {
  }

  ngOnInit(): void {
    this.isSlider = this.blocks.length;
  }

  onBack(): void {
    this.step = this.step === this.blocks.length - 1 ? 0 : this.step + 1;
  }

  onNext(): void {
    this.step = this.step === 0 ? this.blocks.length - 1 : this.step - 1;
  }

  onWindowResize(event): void {
    this.width = event.target.innerWidth;
  }

}
