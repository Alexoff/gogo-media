export class Block {
  public title: string;
  public subtitle: string;
  public image?: string;

  constructor(obj) {
    this.title = obj.title;
    this.subtitle = obj.subtitle;
    this.image = obj.image;
  }
}
