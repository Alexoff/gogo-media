import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { BlockComponent } from './components/block/block.component';
import { BlocksComponent } from './components/blocks/blocks.component';



@NgModule({
  declarations: [HeaderComponent, BlockComponent, BlocksComponent],
  imports: [
    CommonModule
  ],
  exports: [HeaderComponent, BlockComponent, BlocksComponent]
})
export class SharedModule { }
